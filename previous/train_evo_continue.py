from osim.env import GaitEnv
from Evo import Evo
import pandas as pd
from pa import Trainer
import matplotlib.pyplot as plt
import pickle


top = 60

with open('models/evo_2000_top_60.pkl','rb') as ef:
    evo = pickle.load(ef)

env = GaitEnv(visualize=False)
trainer = Trainer(env)

episodes = []
for i_episode in range(int(2000),int(3000)):
    episode, t = trainer.run_once(lambda x:evo.getAction(x))
    print("{}th episode finished after {} timesteps." \
          .format(i_episode, t+1))
    episodes.append(episode)

    if (i_episode+1) % 200 == 0:
        evo = Evo(episodes=episodes,top=top)
        pd.Series([len(x) for x in episodes]).hist(bins=50)
        plt.savefig('figures/{}.png'.format(i_episode+1))
        plt.close()
        episodes = []

    if (i_episode+1) % 1000 == 0:
        with open('models/evo_{}_top_{}.pkl'.format(i_episode+1, top),'wb') as ef:
            pickle.dump(evo,ef)
