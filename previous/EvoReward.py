import numpy as np
from scipy.spatial.distance import cdist
import pickle

class Evo():
    def __init__(self,episodes='data-100.pkl',top=5,n=5):
        self.n = n
        self.factors = np.array([
            1,1,8,1,1,
            1,8,1,1,1,
            1,1,1,1,1,
            1,1,1,1,1,1,
            1,8,1,1,8,
            1,80,8,80,8,
          ])

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        # maybe switch sum of rewards to cumsum?
        episodes = sorted(episodes,key=lambda x:np.sum([y[2] for y in x]),
        reverse=True)[:top]

        for i, episode in enumerate(episodes):
            if i == 0:
                obs, action = self.get(episode)
                self.observations = obs
                self.actions = action
            else:
                self.add(episode)

        self.observationsFactored = self.observations.copy()*self.factors

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action

    def add(self,episode):
        obs, action = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.vstack(( \
            action,
            self.actions
        ))

    def getActionScaled(self,obs):
        factoredObs = obs.copy()
        factoredObs = factoredObs*self.factors
        mat = cdist([factoredObs], self.observationsFactored, 'euclidean')
        return self._getAction(mat)

    def getAction(self,obs):
        mat = cdist([obs], self.observations, 'euclidean')
        return self._getAction(mat)

    def _getAction(self,mat):
        vector = np.squeeze(mat)
        actionMat = self.actions[np.argsort(vector),:]
        actionMat = actionMat[:self.n,:]
        means = np.mean(actionMat,axis=0)
        stds = np.std(actionMat,axis=0)
        action = np.random.normal(means,stds)
        # avoid action with vals < 0 or > 1
        for i, val in enumerate(action):
            if val < 0 or val > 1:
                while True:
                    newVal = np.random.normal(means[i],stds[i])
                    if newVal >=0 and newVal <=1:
                        break
                action[i] = newVal
        return action
