import numpy as np
import os

def createDir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def createDirs(name):
    createDir('figures/'+name)
    createDir('models/'+name)

class Tracker():
    def __init__(self,avgBy=100, greater=False):
        # greater determines if the objective is to get more time steps
        # (as in CartPole) or less time steps (as in Mountain Car)
        self.store = []
        self.avgBy = avgBy
        self.greater = greater
        if self.greater:
            self.best = -np.inf
            self.compareFun = lambda current, best: current>best
        else:
            self.best = np.inf
            self.compareFun = lambda current, best: current<best

    def getAvg(self):
        return np.mean(self.store)

    def isBest(self,t):
        if len(self.store) < self.avgBy:
            self.store.append(t)
            return False
        else:
            self.store.pop(0)
            self.store.append(t)
            avg = self.getAvg()
            if self.compareFun(avg,self.best):
                self.best = avg
                return True
            else:
                return False

class Trainer():
    def __init__(self,env):
        self.env = env

    def run_once(self,actionFun,tracker=None,
    keep_size=None,add_step=False):
        episode = []
        rewards = 0
        obs = self.env.reset()
        for t in range(int(3000)): # don't put 1e10 here too much for the range function
            action = actionFun(obs)
            newObs, reward, done, info = self.env.step(action)
            episode.append([obs,action,reward])
            rewards += reward
            obs = newObs
            if done:
                break

        if tracker:
            tracker.isBest(t+1)

        if add_step:
            for item in episode:
                item.append(t+1)

        if keep_size:
            episode = episode[-keep_size:]

        return episode, t+1, rewards
