from osim.env import GaitEnv
from EvoReward import Evo
import pandas as pd
from pa import Trainer,createDirs
import matplotlib.pyplot as plt
import pickle

tag = 'rewardScaled'
createDirs(tag)

top = 60
evo = Evo(top=top)

env = GaitEnv(visualize=False)
trainer = Trainer(env)

episodes = []
rewardsTotal = []
for i_episode in range(0,int(1e4)):
    episode, t, rewards = trainer.run_once(lambda x:evo.getActionScaled(x))
    print("{}th episode finished after {} timesteps with rewards {}." \
          .format(i_episode+1, t, rewards))
    episodes.append(episode)
    rewardsTotal.append(rewards)

    if (i_episode+1) % 200 == 0:
        evo = Evo(episodes=episodes,top=top)
        pd.Series(rewardsTotal).hist(bins=50)
        plt.savefig('figures/{}/{}.png'.format(tag,i_episode+1))
        plt.close()
        episodes = []
        rewardsTotal = []

    if (i_episode+1) % 1000 == 0:
        with open('models/{}/evo_{}_top_{}.pkl'.format(tag, i_episode+1, top),
                  'wb') as ef:
            pickle.dump(evo,ef)
