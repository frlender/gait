import pickle
import numpy as np
from Evo import Evo
import pandas
import matplotlib.pyplot as plt

with open('data-100.pkl','rb') as df:
    episodes = pickle.load(df)

byReward = sorted(episodes,key=lambda x:np.sum([y[2] for y in x]),reverse=True)
stats = []
for e in episodes:
    stats.append([len(e),np.sum([y[2] for y in e])])
stats = sorted(stats,key=lambda x:x[1],reverse=True)
x,y = zip(*stats)
plt.scatter(x,y)

cc = np.array([
      [1,2],
      [1,3],
      [2,3]
      ])
cd = np.array([1,4])



obs = []
for episode in episodes[:100]:
    for step in episode:
        obs.append(step[0])
obs = np.array(obs)
mean = np.mean(obs,axis=0)
std = np.std(obs,axis=0)
stats = np.vstack((mean,std)).T
factors = [1,1,8,1,1,
           1,8,1,1,1,
           1,1,1,1,1,
           1,1,1,1,1,1,
           1,8,1,1,8,
           1,80,8,80,8,
           ]
stats = np.vstack((mean,std,factors)).T

evo = Evo(top=60)
obsMat = evo.observations
self = evo
obs = obsMat[100,]


with open('evo.pkl','rb') as ef:
    evo = pickle.load(ef)
    
    
actions = []
for episode in episodes[:100]:
    for step in episode:
        actions.append(step[1])
actions = np.array(actions)
action = actions[11,:]
means = np.mean(actions,axis=0)
stds = np.std(actions,axis=0)
for i, item in enumerate(action):
    print(i,item)
for i, val in enumerate(action):
    if val < 0 or val > 1:
        while True:
            newVal = np.random.normal(means[i],stds[i])
            if newVal >=0 and newVal <=1:
                break
        action[i] = newVal