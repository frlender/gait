import numpy as np
from scipy.spatial.distance import cdist
import pickle

class Evo():
    def __init__(self,episodes='data-100.pkl',top=5,n=5):
        self.n = n

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        episodes = sorted(episodes,key=lambda x:len(x),reverse=True)[:top]
        for i, episode in enumerate(episodes):
            if i == 0:
                obs, action = self.get(episode)
                self.observations = obs
                self.actions = action
            else:
                self.add(episode)

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action

    def add(self,episode):
        obs, action = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.vstack(( \
            action,
            self.actions
        ))

    def getAction(self,obs):
        # obs = np.array([obs])
        # factoredObs = obs.copy()
        # factoredObs[:,1] = obs[:,1]*self.factor
        # factoredSelfObs = self.observations.copy()
        # factoredSelfObs[:,1] = self.observations[:,1]*self.factor
        # mat = cdist(factoredObs, factoredSelfObs, 'euclidean')

        mat = cdist([obs], self.observations, 'euclidean')
        vector = np.squeeze(mat)
        actionMat = self.actions[np.argsort(vector),:]
        actionMat = actionMat[:self.n,:]
        means = np.mean(actionMat,axis=0)
        stds = np.std(actionMat,axis=0)
        action = np.random.normal(means,stds)
        # avoid action with vals < 0 or > 1
        for i, val in enumerate(action):
            if val < 0 or val > 1:
                while True:
                    newVal = np.random.normal(means[i],stds[i])
                    if newVal >=0 and newVal <=1:
                        break
                action[i] = newVal
        return action
