from keras.models import Sequential
from keras.layers import Dense, Activation

model = Sequential([
    Dense(900,input_dim=30),
    Activation('relu'),
    Dense(18),
    Activation('sigmoid') # use softmax instead.
])
