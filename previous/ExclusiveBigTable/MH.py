import numpy as np

def getF(fixed):
    def f(x):
        squaredSum = np.min(np.sum(np.square(x-fixed),axis=1))
        res = np.exp(-1/squaredSum)
        return res
    return f

def mhSample(fixed,cov_scale=1, thres=1e3):
    # Sample a complex distribution defined by f(x) using
    # the Metropolis-Hastings algorithm.
    # fixed: in the shape (samples, dims)
    # cov_scale: how much to scale cov
    # thres: sample thres times before termination
    f = getF(fixed)
    cov = np.cov(fixed.T)*cov_scale
    cov = np.diag(np.diag(cov))

    count = 0
    # the action_space is [0,1] for all dims
    x = np.random.uniform(size=fixed.shape[1])
    for i in range(int(1e4)): # upper limit for running
        while True:
            xNew = np.random.multivariate_normal(x,cov)
            # check range
            if np.all(np.bitwise_and(xNew>=0,xNew<1)):
                break
        alpha = f(xNew)/f(x)
        if alpha >= 1:
            x = xNew
            count += 1
            if count == thres:
                break
        else:
            mark = np.random.uniform()
            if alpha > mark:
                x = xNew
                count += 1
                if count == thres:
                    break

    return xNew
