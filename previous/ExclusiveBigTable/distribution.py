import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#fixed = np.random.uniform(size=2)
fixed = np.array([0.5,0.7,0.3,0.8,0.85])

std = np.std(fixed)*2
def f(x):
    squaredSum = np.sum(np.square(x-fixed))
    res = np.exp(-1/squaredSum)
    
#    dist = np.min(np.square(x-fixed))
#    res = np.exp(-1/dist)
    return res

samples = []
x = np.random.uniform()
for i in range(int(1e4)):
    while True:
        xNew = np.random.normal(x,std)
        # check range
        if xNew >=0 and xNew <=1:
            break
    alpha = f(xNew)/f(x)
    print(alpha)
    if alpha >= 1:
        x = xNew
        samples.append(xNew)
    else:
        mark = np.random.uniform()
        if alpha > mark:
            x = xNew
            samples.append(xNew)

plt.clf()
pd.Series(samples[100:]).hist(bins=200)
