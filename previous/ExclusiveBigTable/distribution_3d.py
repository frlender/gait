import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd


fixed = np.array([[0.1,0.1,0.1],[0.9,0.9,0.9],[0.5,0.7,0.9],
                  [0.7,0.8,0.7],[0.2,0.1,0.3]])
# fixed = np.array([[0.7,0.8,0.7],[0.2,0.1,0.3],[0.5,0.7,0.9]])


def getF(fixed):
    def f(x):
#        squaredSum = np.sum(np.square(x-fixed)) # add cov?
        # mean, median, min are all viable choices. Min is the most stringent.
        # Looks like min works best for high dimensions
        squaredSum = np.min(np.sum(np.square(x-fixed),axis=1))
        res = np.exp(-1/squaredSum)
        return res
    return f

f = getF(fixed)
cov_scale=0.08
cov = np.cov(fixed.T)*cov_scale
cov = np.diag(np.diag(cov))

samples = []
# the action_space is [0,1] for all dims
x = np.random.uniform(size=fixed.shape[1])
for i in range(int(1e4)): # upper limit for running
    if i%100 == 0:
        print(i)
    while True:
        xNew = np.random.multivariate_normal(x,cov)
        # check range
        if np.all(np.bitwise_and(xNew>=0,xNew<1)):
            break
    alpha = f(xNew)/f(x)
    if alpha >= 1:
        x = xNew
        samples.append(xNew)
    else:
        mark = np.random.uniform()
        if alpha > mark:
            x = xNew
            samples.append(xNew)

plt.clf()
x,y,z = list(zip(*samples[100:]))
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d',alpha=0.3)
ax.scatter(x, y, z, c='b', marker='o')


x0,y0,z0 = list(zip(*fixed))
plt.scatter(x0,y0,10,c='r', marker='0')
