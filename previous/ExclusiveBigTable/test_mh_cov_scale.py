import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
from Memo import Memo
from scipy.spatial.distance import cdist


memo = Memo(limit=300)

obs = np.squeeze(memo.sample(1))
mat = cdist([obs],memo.observations, 'euclidean')
vector = np.squeeze(mat)
actionMat = memo.actions[np.argsort(vector),:]
fixed = actionMat[:memo.n,:]

total = []
for i in np.arange(1,3,1):
    cov_scale=i
    cov = np.cov(fixed.T)*cov_scale
    cov = np.diag(np.diag(cov))

    counts = []
    for j in range(100):
        print(i,j)
        count = 0
        x = np.random.uniform(size=fixed.shape[1])
        while True:
            count +=1
            xNew = np.random.multivariate_normal(x,cov)
            if np.all(np.bitwise_and(xNew>=0,xNew<1)):
                break
        counts.append(count)
    total.append(counts)

total = np.array(total)
means = np.mean(total,axis=1)
stds = np.std(total,axis=1)
stats = np.vstack((means,stds)).T