#from osim.env import GaitEnv
#import numpy as np
#
#
#env = GaitEnv(visualize=False)
#
#scale = 0.05
#
#def getEach(val,scale):
#    while True:
#        nextVal = np.random.normal(val,scale)
#        if nextVal > 0 and nextVal < 1:
#            break
#    return nextVal
#
#def getAction(action,scale):
#    return np.array([getEach(x,scale) for x in action])
#    
#
#observation = env.reset()
#action = env.action_space.sample()
#observation, reward, done, info = env.step(action)
#for i in range(299):
#    nextAction = getAction(action,scale)
#    observation, reward, done, info = env.step(nextAction)
#    if reward < -0.15:
#        break
#    action = nextAction
#    print(i, reward)
#
#observation = env.reset()
#for i in range(300):
#    cc = np.zeros(18)
#    cc[7] = 1
#    res = env.step(cc)



#%%
import pickle
import numpy as np
from Memo import Memo
from scipy.spatial.distance import cdist
from MH import mhSample


#with open('data.pkl','rb') as ef:
#    episodes = pickle.load(ef)
#    
#obs, action, _,  = zip(*episode)
#action = np.vstack(action)

memo = Memo(limit=300)
obs = memo.sample()[0]
action = memo.getAction(obs)

#%%
#def matSquare(vec,cov):
#    vec = np.array(vec)
#    cov = np.array(cov)
#    return np.dot(np.dot(vec,cov), vec)
#
#vec = [1,2]
#cov = [[1,0],[0,1]]
#print(matSquare(vec,cov))
#
#cov = np.cov([[1,2],[5,4]])
#vec1 = [1,2]
#vec2 = [2,7]
#print(np.dot(vec2,vec2)/np.dot(vec1,vec1))
#print(matSquare(vec2,cov)/matSquare(vec1,cov))