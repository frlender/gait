import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd

# potentially explore in 3d

fixed = np.array([[0.7,0.8],[0.2,0.1],[0.5,0.7]])

def getF(fixed):
    def f(x):
#        squaredSum = np.sum(np.square(x-fixed)) # add cov?
        # mean, median, min are all viable choices. Min is the most stringent.
        squaredSum = np.mean(np.sum(np.square(x-fixed),axis=1))
        res = np.exp(-1/squaredSum)
        return res
    return f

f = getF(fixed)
cov_scale=1
cov = np.cov(fixed.T)*cov_scale
cov = np.diag(np.diag(cov))

samples = []
# the action_space is [0,1] for all dims
x = np.random.uniform(size=fixed.shape[1])
for i in range(int(1e4)): # upper limit for running
    if i%100 == 0:
        print(i)
    while True:
        xNew = np.random.multivariate_normal(x,cov)
        # check range
        if np.all(np.bitwise_and(xNew>=0,xNew<1)):
            break
    alpha = f(xNew)/f(x)
    if alpha >= 1:
        x = xNew
        samples.append(xNew)
    else:
        mark = np.random.uniform()
        if alpha > mark:
            x = xNew
            samples.append(xNew)

plt.clf()
x,y = list(zip(*samples[100:]))
plt.scatter(x,y,10,alpha=0.3)
x0,y0 = list(zip(*fixed))
plt.scatter(x0,y0,10,c='r')

#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#hist, xedges, yedges = np.histogram2d(x, y, bins=(15,15))
#xpos, ypos = np.meshgrid(xedges[:-1]+xedges[1:], yedges[:-1]+yedges[1:])
#
#xpos = xpos.flatten()/2.
#ypos = ypos.flatten()/2.
#zpos = np.zeros_like (xpos)
#
#dx = xedges [1] - xedges [0]
#dy = yedges [1] - yedges [0]
#dz = hist.flatten()
#
#ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color='b', zsort='average')
#plt.xlabel ("X")
#plt.ylabel ("Y")
#
#plt.show()

#%%
#t = []
#for i in range(1000):
#    t.append(np.random.multivariate_normal([0,0],[[0.08,0.04],[0.04,0.02]]))
#pd.DataFrame(t).plot.scatter(0,1)