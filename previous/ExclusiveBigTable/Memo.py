import numpy as np
from scipy.spatial.distance import cdist
import pickle
from random import shuffle
from MH import mhSample

#IDEA remove existing entries by timesteps. Those with large timesteps are
#removed first

class Memo():
    def __init__(self,episodes='data.pkl',limit=3000, n=5,
    span=1, keep_size=10, factor=1, use_shuffle=True,
    forget_by_steps=False, mh_cov_scale=0.5, mh_thres=1e2):
        args = locals()
        del args['self']
        self.__dict__.update(args)

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        if use_shuffle:
            shuffle(episodes)

        for i, episode in enumerate(episodes):
            # print(i)
            if len(episode) > keep_size:
                episode = episode[-self.keep_size:]
            for item in episode:
                item.append(len(episode))
            if i==0:
                episode = self.select(episode)
                obs, action, step = self.get(episode)
                self.observations = obs
                self.actions = action
                self.steps = step
            else:
                self.add(episode)
            if self.observations.shape[0] == self.limit:
                break

    def select(self,episode):
        start = np.random.randint(1,self.span+1)
        return [episode[i] for i in range(-start,-len(episode)-1,-self.span)]

    def get(self,episode):
        obs, action, _, step = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        step = np.hstack(step)
        return obs, action, step

    def add(self,episode):
        episode = self.select(episode)
        obs, action, step = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.vstack(( \
            action,
            self.actions
        ))
        self.steps = np.hstack(( \
            step,
            self.steps
        ))
        if self.observations.shape[0] > self.limit:
            if self.forget_by_steps:
                # remove episode of large steps
                idx = np.argsort(self.steps)[:self.limit]
            else:
                idx = range(self.limit)
            self.observations = self.observations[idx,:]
            self.actions = self.actions[idx,:]
            self.steps = self.steps[idx]

    def sample(self,size=1):
        idx = np.random.choice(self.observations.shape[0],
        size=size, replace=False)
        return self.observations[idx,:]

    def getAction(self,obs):
        # input is one dim vector
        mat = cdist([obs],self.observations, 'euclidean')
        vector = np.squeeze(mat)
        actionMat = self.actions[np.argsort(vector),:]
        actionMat = actionMat[:self.n,:]
        action = mhSample(actionMat,self.mh_cov_scale,
        self.mh_thres)
        return action

    def getActions(self,obs):
        # maybe scale velocity? since velocity is 10 times smaller than position
        # or use cosine distance? Looks like euclidean is better than cosine
        # obs = np.array(obs)
        # factoredObs = obs.copy()
        # factoredObs[:,1] = obs[:,1]*self.factor
        # factoredSelfObs = self.observations.copy()
        # factoredSelfObs[:,1] = self.observations[:,1]*self.factor
        #
        # mat = cdist(factoredObs, factoredSelfObs, 'euclidean')

        mat = cdist(obs,self.observations, 'euclidean')
        actionTensor = self.actions[np.argsort(mat)]
        #print(actionMat)
        actionTensor = actionTensor[:,:self.n,:]
        # @TODO to be finished using np.apply_over_axes() ...

        # actions = np.apply_along_axis(
        #     lambda x: np.random.normal(np.mean(x),np.std(x)),
        #     axis=1,
        #     arr=actionMat
        # )
        # actions = np.apply_along_axis(
        #         lambda x: np.bincount(x, minlength=3),
        #         axis=1,
        #         arr=actionMat
        #     ).argmax(axis=1)
        # return actions

    def getActionsE(self,episode):
        obs, _, _ = self.get(episode)
        return self.getActions(obs)
