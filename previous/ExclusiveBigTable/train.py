from osim.env import GaitEnv

from Memo import Memo

keep_size = 20

memo = Memo(limit=3000,n=5,span=1,keep_size=keep_size,forget_by_steps=False)

env = GaitEnv(visualize=False)

for i_episode in range(int(500)):
    episode = []
    obs = env.reset()
    for t in range(int(2000)):
        if t%10 == 0:
            print(t)
        action = memo.getAction(obs)
#        action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        episode.append([obs,action,reward])
        obs = newObs
        if reward < -0.15 or done:
            print("{}th episode finished after {} timesteps."\
            .format(i_episode, t+1))
            break

    for item in episode:
        item.append(t)

    if len(episode) > keep_size:
        episode = episode[-keep_size:]
    memo.add(episode)
