import numpy as np
import pickle

def getMaxReturn(episode):
    cumsum = np.cumsum([x[2] for x in episode])
    idx = np.argmax(cumsum)
    return idx, cumsum[idx]

def transform(episode):
    idx, _ = getMaxReturn(episode)
    return episode[:(idx+1)]

def clean(episodes):
    epis = list(map(transform,episodes))
    epis = sorted(epis, key=lambda x:-getMaxReturn(x)[1])
    rets = [getMaxReturn(x)[1] for x in epis]
    return epis, rets, np.mean(rets)

def loadPkl(path):
    with open(path,'rb') as pf:
        return pickle.load(pf)

def printEvery(unit,n,string=None):
	if n%unit==0:
		if string:
			print(string)
		else:
			print(n)
