from osim.env import GaitEnv
import pickle
import pa
import pandas as pd
import match as m




env = GaitEnv(visualize=False)
trainer = pa.Trainer(env)

episodes = []
for i_episode in range(int(200)):
    episode, t = trainer.run_once(lambda x:env.action_space.sample())
    print("{}th episode finished after {} timesteps." \
          .format(i_episode, t+1))
    episodes.append(episode)

epis, rets, avgRet = m.clean(episodes)
with open('data-top-20.pkl','wb') as df:
    pickle.dump(epis[:20],df)

with open('data.pkl','wb') as df:
    pickle.dump(epis,df)


#with open('data.pkl','rb') as df:
#    episodes = pickle.load(df)
#
#pd.Series([len(x) for x in episodes]).hist(bins=100)
#    
#cc = sorted(episodes,key=lambda x:len(x),reverse=True)
#
#with open('data-100.pkl','wb') as df:
#    pickle.dump(episodes[:100],df)