from osim.env import GaitEnv
from Evo import Evo
import pandas as pd
from pa import Trainer
import matplotlib.pyplot as plt
import pickle
import match as m



env = GaitEnv(visualize=True)
trainer = Trainer(env)

evo = m.loadPkl('models/1st/20_263.37.pkl')
episode, t = trainer.run_once(lambda x,t:evo.getAction(x,0))
ret = m.getMaxReturn(episode)
print("{}th episode finished after {} timesteps." \
.format(1, t+1))

plt.plot([x[0][19] for x in episode])

rewards = [x[2] for x in episode]
plt.plot([x[2] for x in episode])
