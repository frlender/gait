from osim.env import GaitEnv
import pickle

from Evo import Evo
from pa import Trainer,createDirs
import match as m

tag = '1st'
createDirs(tag)

top = 20
rf_n = 20
session_len = 200
eps = lambda x:30.0/(50+x)

env = GaitEnv(visualize=False)
trainer = Trainer(env)

priorEpis = m.loadPkl('data-prior.pkl')
evo = Evo('data-prior.pkl',rf_n=rf_n)
best = 0
for i in range(0,500):
    episodes = []
    n = 0
    while(len(episodes)<session_len):
        episode, t = trainer.run_once(
            lambda obs,t:evo.getAction(obs,eps(i)),
        )
        episodes.append(episode)
        m.printEvery(50, n+1,
        'in session: {}th, {} epis'.format(n+1,len(episodes)))
        n += 1

    epis, rets, avgRet = m.clean(episodes)
    print('{}th session, avgRet {:.2f}, best ret {:.2f}'.format(
    i+1, avgRet, rets[0]))

    if avgRet > best:
        best = avgRet
        with open('models/{}/{}_{:.2f}.pkl'.format(tag,i+1,avgRet),
        'wb') as mf:
            pickle.dump(evo,mf)

    if avgRet < 480:
        evo = Evo(epis[:top]+priorEpis,rf_n=rf_n)
    else:
        evo = Evo(epis[:top],rf_n=rf_n)
