from osim.env import GaitEnv
from Evo import Evo
import pandas as pd
from pa import Trainer
import matplotlib.pyplot as plt
import pickle
import match as m
import numpy as np


env = GaitEnv(visualize=True)
trainer = Trainer(env)

evo = m.loadPkl('models/1st/28_101.85.pkl')
#%%
def mods(vec,mod=None):
    if mod == 'pull':
        vec[2] = 0
        vec[3] = 1
#        vec[4] = 1
        vec[5] = 0
    elif mod == 'relax':
        vec[2] = 0
        vec[3] = 0
        vec[5] = 0
    elif mod == 'push':
        vec[3] = 0
        vec[7] = 1
        vec[2] = 1
    elif mod == 'push2':
        vec[3] = 0
        vec[7] = 0.15
        vec[2] = 1
    elif mod == 'push3':
        vec[3] = 0
        vec[7] = 0.08
        vec[2] = 1
    elif mod == 'push4':
        vec[3] = 0
        vec[7] = 0
        vec[2] = 1

def actionFun(x,t):
    programLeft = [
        [0,60,'pull'],
        [80,150,'push2'],
        [150,205,'pull'],
        [210,260,'push3'],
#        [270,280,'push'],
        [330,370,'pull']
    ]
    programRight = [
#        [0,60,'push'],
        [50,75,'push'],
        [80,140,'pull'],
        [170,200,'push2'],
        [260,300, 'pull'],
        [300,390,'push4'],
#        [415,420,'push']
    ]
    action = evo.getAction(x,0)
    for item in programRight:
        if t>= item[0] and t < item[1]:
            print(t,item)
            mods(action[:9],item[2])
            
    for item in programLeft:
        if t>= item[0] and t < item[1]:
            print(t,item)
            mods(action[9:],item[2])
    return action
#%%
episode, t = trainer.run_once(actionFun,max_iter=500)
print("{}th episode finished after {} timesteps." \
.format(1, t+1))
epis, rets, _ = m.clean([episode])
with open('data-prior.pkl','wb') as df:
    pickle.dump(epis,df)

#%%
def actionFun(x):
    action = evo.getAction(x,0)
#    action[0] = 0
#    action[1] = 0
    action[2] = 0
    action[3] = 1
#    action[4] = 0
    action[5] = 0
#    action[6] = 0
    return action
